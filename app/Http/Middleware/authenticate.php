<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
         if (!Sentinel::check()) {
             return abort(404, 'Unauthorized action.');
         }

         return $next($request);
     }
}
