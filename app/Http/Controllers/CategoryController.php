<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use Sentinel;
use Validator;
use Carbon\Carbon;
use Session;

class CategoryController extends Controller
{
     protected function validator(array $data)
    {
        return Validator::make($data, [
            'category_name' => 'required|max:255',
        ]);
    }

    public function add_category()
    {
        $data['categories'] = Categories::all();
        return view('back-template.partials.add_category')->with($data);
    }

    public function insert_category(Request $request)
    {
        $fields = $request->all();

        $validator = $this->validator($fields);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {

        $data = array(
        'category_name' => $fields['category_name'],
        'category_slug' => str_slug($fields['category_name']),
        'created_at' => Carbon::now(),
        'user_id'       => Sentinel::check()->id,
        );

        $insert = Categories::insert($data);

         if($insert > 0)
            {
            Session::flash('messages','<div class="alert-success" style="padding: 5px;margin: 7px 0 4px 0;border-radius: 5px;font-weight: bold;"><center>New Category Has Been Inserted.</center></div>');
            return redirect()->back();

            }

        }
    }

}
