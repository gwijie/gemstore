<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use Validator;
use Carbon\Carbon;
use Image;
use Sentinel;
use Session;

class FrontController extends Controller
{
  public function index()
  {
    $data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
    return view('front-template.partials.home')->with($data);
  }

  public function about()
  {
    //$data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
    return view('front-template.partials.about');//->with($data);
  }

  public function store()
  {
    $data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
    $data['categories'] = Categories::get();
    return view('front-template.partials.shop')->with($data);
  }

  public function contact()
  {
    //$data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
    return view('front-template.partials.contacts');//->with($data);
  }

  public function product_detail($id)
  {
    $data['product'] = Products::join('categories','categories.cat_id','=','products.category')
    ->where('products.product_id','=',$id)
    ->first();
    return view('front-template.partials.product_detail')->with($data);
  }

}
