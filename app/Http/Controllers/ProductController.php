<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Categories;
use App\Products;
use Validator;
use Carbon\Carbon;
use Image;
use Sentinel;
use Session;

class ProductController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'product_name' => 'required',
            'colour' => 'required',
            'category' => 'required',
            'dimensions' => 'required',
            'image' => 'image',
            'description' => 'required',
            'price' => 'required',
            'carats' => 'required',
            'cut' => 'required'
        ]);
    }

    public function add_products()
    {
        $data['categories'] = Categories::where('status','=','1')->get();
        $data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
        return view('back-template.partials.add_product')->with($data);
    }

    public function show()
    {
        $data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
        return view('back-template.partials.view_products')->with($data);
    }

    public function showItem($id)
    {
        $data['product'] = Products::join('categories','categories.cat_id','=','products.category')
        ->where('products.product_id','=',$id)
        ->first();
        return view('back-template.partials.product_view')->with($data);
    }

    public function editItem($id)
    {
        $data['categories'] = Categories::where('status','=','1')->get();
        $data['products'] = Products::join('categories','categories.cat_id','=','products.category')->get();
        $data['product'] = Products::join('categories','categories.cat_id','=','products.category')
        ->where('products.product_id','=',$id)
        ->first();
        return view('back-template.partials.edit_product')->with($data);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails())
    		{
    			return redirect()->back()->withErrors($validator)->withInput();
    		}
        else
        {

        $file = \Input::file('image');

        $image = \Image::make(Input::file('image'));

        $originalImg = public_path().'/img/original/';

        $fileextension = $file->getClientOriginalExtension();

        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $image->save($originalImg.$filename.'.'.$fileextension);

        $image->resize(270, null, function ($constraint)
                          {
                              $constraint->aspectRatio();
                          });

        $fileextension = $file->getClientOriginalExtension();

        $resizeImg = public_path().'/img/thumbnail/';

        $image->save($resizeImg.'thumb_'.$filename.'.'.$fileextension);

        $data = array(
            'product_name' => $fields['product_name'],
            'cut' => $fields['cut'],
            'category' => $fields['category'],
            'dimensions' => $fields['dimensions'],
            'image' => $filename.'.'.$fileextension,
            'description' => $fields['description'],
            'color' => $fields['colour'],
            'price' => $fields['price'],
            'carats' => $fields['carats'],
            'created_at' => Carbon::now(),
            'user_id' => Sentinel::check()->id,
        );

            //dd($data);

            $product = Products::insert($data);

            if($product > 0)
            {
              Session::flash('messages','<div class="alert-success" style="padding: 5px;margin: 7px 0 4px 0;border-radius: 5px;font-weight: bold;"><center>New Item has been added!</center></div>');
              return redirect()->back();
            }

        }
    }

    public function update_product($id,Request $request)
    {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails())
    		{
    			return redirect()->back()->withErrors($validator)->withInput();
    		}
        else
        {
          if(Input::file('image') != '')
            {
                $file = \Input::file('image');

                $image = \Image::make(Input::file('image'));

                $originalImg = public_path().'/img/original/';

                $fileextension = $file->getClientOriginalExtension();

                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                $image->save($originalImg.$filename.'.'.$fileextension);

                $image->resize(270, null, function ($constraint)
                                  {
                                      $constraint->aspectRatio();
                                  });

                $fileextension = $file->getClientOriginalExtension();

                $resizeImg = public_path().'/img/thumbnail/';

                $image->save($resizeImg.'thumb_'.$filename.'.'.$fileextension);

                $data = array(
                    'product_name' => $fields['product_name'],
                    'cut' => $fields['cut'],
                    'category' => $fields['category'],
                    'dimensions' => $fields['dimensions'],
                    'image' => $filename.'.'.$fileextension,
                    'description' => $fields['description'],
                    'color' => $fields['colour'],
                    'price' => $fields['price'],
                    'carats' => $fields['carats'],
                    'created_at' => Carbon::now(),
                    'user_id' => Sentinel::check()->id,
                );


              }
              else
              {

                $data = array(
                    'product_name' => $fields['product_name'],
                    'cut' => $fields['cut'],
                    'category' => $fields['category'],
                    'dimensions' => $fields['dimensions'],
                    //'image' => $filename.'.'.$fileextension,
                    'description' => $fields['description'],
                    'color' => $fields['colour'],
                    'price' => $fields['price'],
                    'carats' => $fields['carats'],
                    'created_at' => Carbon::now(),
                    'user_id' => Sentinel::check()->id,
                );

              }

              $product = Products::where('product_id',$id)->update($data);

              if($product > 0)
              {
                Session::flash('messages','<div class="alert-success" style="padding: 5px;margin: 7px 0 4px 0;border-radius: 5px;font-weight: bold;"><center>Product Item has been Updated!</center></div>');
                return redirect()->back();
              }
        }
    }
}
