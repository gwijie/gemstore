<?php

namespace App\Http\Controllers\Auth;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Session;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
    }

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function index()
    {
        return view('auth.login');
    }

    public function logout()
    {
        Sentinel::logout();

        return redirect('/');
    }

    public function postLogin(Request $request)
    {

        $fields = $request->all();

        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($v->errors())->withInput();
        }
        else
        {


            Sentinel::authenticate($fields);

            if (Sentinel::check())
            {
                if(Sentinel::check())
                {
                    return redirect('/dashboard');
                }
                else
                {
                    Sentinel::logout();

                    Session::flash('messages','<div class="alert-danger" style="padding: 5px;margin: 7px 0 4px 0;border-radius: 5px;font-weight: bold;"><center>Invalid Username or Password</center></div>');
                    return redirect()->back();
                }
            }
            else
            {
                Session::flash('messages','<div class="alert-danger" style="padding: 5px;margin: 7px 0 4px 0;border-radius: 5px;font-weight: bold;"><center>Invalid Username or Password</center></div>');
                return redirect()->back();
            }


            //dd($request->all());
        }
    }
}
