<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Generation Gems EA | Login | Restricted Area </title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <!-- ionicons.css -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('css/custom.min.css')}}" rel="stylesheet">
  </head>

    <div class="row">
      <?php echo Session::get('messages');?>
    </div>

  <body class="login">
    <div>


      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" action="/authenticate">
            {{ csrf_field() }}
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" name="email" placeholder="Username" required="" />
                  @if($errors->first('email'))
                      <p class="" style="color: red;">
                      {{ $errors->first('email') }}
                      </p>
                      @endif
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                  @if($errors->first('password'))
                      <p class="" style="color: red;">
                      {{ $errors->first('password') }}
                      </p>
                      @endif
              </div>
              <div>
                <button type="submit" class="btn btn-default submit" >Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="/register" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-diamond"></i>{{ config('app.name') }}</h1>
                  <p>©{{date('Y')}} All Rights Reserved. </p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
