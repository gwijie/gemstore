<!-- footer content -->
      <footer>
        <div class="pull-right">
          Developed & Maintained by <a href="#">Creative Atlier</a>
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>


  <!-- jQuery -->
  <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <!-- FastClick -->
  <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
  <!-- NProgress -->
  <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
  <!-- Chart.js -->
  <script src="{{asset('vendors/Chart.js/dist/Chart.min.js')}}"></script>
  <!-- jQuery Sparklines -->
  <script src="{{asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>

  <!-- bootstrap-wysiwyg -->
  <script src="{{asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>

  <!-- Flot -->
  <script src="{{asset('vendors/Flot/jquery.flot.js')}}"></script>
  <script src="{{asset('vendors/Flot/jquery.flot.pie.js')}}"></script>
  <script src="{{asset('vendors/Flot/jquery.flot.time.js')}}"></script>
  <script src="{{asset('vendors/Flot/jquery.flot.stack.js')}}"></script>
  <script src="{{asset('vendors/Flot/jquery.flot.resize.js')}}"></script>
  <!-- Flot plugins -->
  <script src="{{asset('js/flot/jquery.flot.orderBars.js')}}"></script>
  <script src="{{asset('js/flot/date.js')}}"></script>
  <script src="{{asset('js/flot/jquery.flot.spline.js')}}"></script>
  <script src="{{asset('js/flot/curvedLines.js')}}"></script>
  <!-- bootstrap-daterangepicker -->
  <script src="{{asset('js/moment/moment.min.js')}}"></script>
  <script src="{{asset('js/datepicker/daterangepicker.js')}}"></script>

<!-- Datatables -->
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
  <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
  <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
  <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>

    <!-- Bootstrap Colorpicker -->
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>

  <!-- Custom Theme Scripts -->
  <script src="{{asset('build/js/custom.js')}}"></script>

      <!-- bootstrap-wysiwyg -->
  <script>
    $(document).ready(function() {
      //colour picker
      $(".demo2").colorpicker();

      //ckeditor
			CKEDITOR.replace( 'description' );


    });
  </script>
  <!-- /bootstrap-wysiwyg -->

  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      var handleDataTableButtons = function() {
        if ($("#datatable-buttons").length) {
          $("#datatable-buttons").DataTable({
            dom: "Bfrtip",
            buttons: [
              {
                extend: "copy",
                className: "btn-sm"
              },
              {
                extend: "csv",
                className: "btn-sm"
              },
              {
                extend: "excel",
                className: "btn-sm"
              },
              {
                extend: "pdfHtml5",
                className: "btn-sm"
              },
              {
                extend: "print",
                className: "btn-sm"
              },
            ],
            responsive: true
          });
        }
      };

      TableManageButtons = function() {
        "use strict";
        return {
          init: function() {
            handleDataTableButtons();
          }
        };
      }();

      $('#datatable').dataTable();
      $('#datatable-keytable').DataTable({
        keys: true
      });

      $('#datatable-responsive').DataTable();

      $('#datatable-scroller').DataTable({
        ajax: "js/datatables/json/scroller-demo.json",
        deferRender: true,
        scrollY: 380,
        scrollCollapse: true,
        scroller: true
      });

      var table = $('#datatable-fixed-header').DataTable({
        fixedHeader: true
      });

      TableManageButtons.init();
    });
  </script>
  <!-- /Datatables -->

</body>
</html>
