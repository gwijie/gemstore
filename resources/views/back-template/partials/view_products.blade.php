@extends('back-template.template')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>All Product</h3>
              </div>

            </div>
            <div class="clearfix"></div>

              <div class="row">{!! Session::get('messages')!!}</div>

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Recently Added Products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>


                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Product Name</th>
                          <th>Carats</th>
                          <th>Category</th>
                          <th>Dimensions (mm)</th>
                          <th>Price/Carat</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                           @foreach($products as $product)
                        <tr>
                          <td>{{$product->product_name}}</td>
                          <td>{{$product->carats}}</td>
                          <td>{{$product->category_name}}</td>
                          <td>{{$product->dimensions}}</td>
                          <td>USD ${{$product->price}}</td>
                          <td>
                              @if($product->status==1)
                              {{'Available'}}
                              @else
                              {{'Unavailable'}}
                              @endif

                          <td><a href="/admin/view_product/{{$product->product_id}}" class="btn btn-sm btn-success"> <i class="fa fa-eye" aria-hidden="true"></i> View</a>
                            <a href="/admin/product/edit/{{$product->product_id}}" class="btn btn-sm btn-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Suspended</a>
                            <a href="#" class="btn btn-sm btn-warning"><i class="fa fa-star" aria-hidden="true"></i> Feature</a>
                          </td>
                        </tr>
                            @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
