@extends('back-template.template')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Category</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="row">{!! Session::get('messages')!!}</div>

            <div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Category <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left input_mask" method="post" action="/add_category">
                    {{ csrf_field()}}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" name="category_name" placeholder="Category Name">

                      @if($errors->first('category_name'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{$errors->first('category_name')}}
                      </span>
                      @endif

                        </div>
                      </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 ">
                          <button type="submit" class="btn btn-success pull-right">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Categories <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <table class="table">
                      <thead>
                        <tr>
                          <th width="350">Category Name</th>
                          <th>Operation</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($categories as $category)
                        <tr>
                          <td>{{$category->category_name}}</td>
                          <td><a href="" class="btn btn-primary btn-sm">Edit</a> <a href="" class="btn btn-sm btn-danger">Delete</a></td>
                        </tr>
                          @endforeach

                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
