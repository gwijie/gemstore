@extends('back-template.template')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>New Product</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

              <div class="row">{!! Session::get('messages')!!}</div>

            <div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Product <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left input_mask" method="post" enctype="multipart/form-data" action="/add_products">
                    {{ csrf_field()}}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" name="product_name" placeholder="Product Name">
                      @if($errors->first('product_name'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('product_name') }}
                      </span>
                      @endif
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select id="" name="category" class="form-control">
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                              <option value="{{$category->cat_id}}">{{$category->category_name}}</option>
                            @endforeach
                          </select>
                      @if($errors->first('category'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('category') }}
                      </span>
                      @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Dimension</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" name="dimensions" class="form-control" placeholder="">
                            @if($errors->first('dimensions'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('dimensions') }}
                      </span>
                      @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Carats</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" name="carats" class="form-control" placeholder="">
                            @if($errors->first('carats'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('carats') }}
                      </span>
                      @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cut</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" name="cut" class="form-control" placeholder="">
                            @if($errors->first('cut'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('cut') }}
                      </span>
                      @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Colour</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="input-group demo2">
                                <input type="text" value="#fff" name="colour" class="form-control" />
                                <span class="input-group-addon"><i></i></span>
                          </div>
                            @if($errors->first('colour'))
                            <span class="label label-danger" style="font-size: 12px!important;">
                            {{ $errors->first('colour') }}
                            </span>
                      @endif
                        </div>
                      </div>

                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Upload</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="file" name="image" >
                            @if($errors->first('image'))
                            <span class="label label-danger" style="font-size: 12px!important;">
                            {{ $errors->first('image') }}
                            </span>
                            @endif
                        </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Image Upload 2</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="file" name="image2" >
                          @if($errors->first('image2'))
                          <span class="label label-danger" style="font-size: 12px!important;">
                          {{ $errors->first('image2') }}
                          </span>
                          @endif
                      </div>
                    </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">

                      <textarea name="description" class="form-control" rows="5" placeholder=''></textarea>
                     @if($errors->first('description'))
                      <span class="label label-danger" style="font-size: 12px!important;">
                      {{ $errors->first('description') }}
                      </span>
                      @endif
                        </div>
                      </div>

                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Price (SRP)</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" name="price" class="form-control" placeholder="">
                          @if($errors->first('price'))
                          <span class="label label-danger" style="font-size: 12px!important;">
                          {{ $errors->first('price') }}
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

                          <button type="submit" class="btn btn-success pull-right">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>


              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Recently Added Products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <table class="table">
                      <thead>
                        <tr>
                          <th>Product Name</th>
                          <th>Category</th>
                          <th>Carats (Cts)</th>
                          <th>Operation</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($products as $product)
                        <tr>
                          <th>{{$product->product_name}}</th>
                          <td>{{$product->category_name}}</td>
                          <td>{{$product->carats}}</td>
                          <td><a href="/admin/view_product/{{$product->product_id}}" class="btn btn-sm btn-success"> <i class="fa fa-eye" aria-hidden="true"></i> View</a>
                            <a href="/admin/product/edit/{{$product->product_id}}" class="btn btn-sm btn-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Susp</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
