@extends('back-template.template')

@section('content')

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left hide">
                <h3></h3>
              </div>

              <div class="title_right hide">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{{$product->carats}} cts {{$product->product_name}}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-7 col-sm-7 col-xs-12">
                      <div class="product-image">
                        <img src="{{asset('/img/original/'.$product->image)}}" alt="{{$product->product_name}}" />
                      </div>
                      <div class="product_gallery hide">
                        <a>
                          <img src="images/prod2.jpg" alt="..." />
                        </a>
                      </div>
                    </div>

                    <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">

                      <h3 class="prod_title">{{$product->carats}} Cts {{$product->product_name}}</h3>

                      <p>{!!$product->description!!}</p>

                      <br />

                      <div class="">
                        <h2>Dimensions</h2>
                        <ul class="list-inline prod_color">
                          <li>
                            <div class="">{{$product->dimensions}}</div>
                          </li>

                        </ul>
                      </div>
                      <br />

                      <div class="">
                        <h2>Cut</h2>
                        <ul class="list-inline prod_color">
                          <li>
                            <div>{{$product->cut}}</div>
                          </li>

                        </ul>
                      </div>
                      <br />

                      <div class="">
                        <h2>Color</h2>
                        <ul class="list-inline prod_color">
                          <li>
                            <div class="color" style="background-color: {{$product->color}}"></div>
                          </li>

                        </ul>
                      </div>

                      <br />

                      <div class="">
                        <div class="product_price">
                          <h1 class="price">$ USD {{$product->price}} / Carat</h1>
                          <br>
                        </div>
                      </div>

                      <div class="hide">
                        <button type="button" class="btn btn-default btn-lg">Add to Cart</button>
                      </div>

                      <div class="product_social">
                        <ul class="list-inline">
                          <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-envelope-square"></i></a>
                          </li>
                        </ul>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
