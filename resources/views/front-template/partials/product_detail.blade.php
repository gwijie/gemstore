@extends('front-template.template')

@section('content')

<div class="clearfix"></div>
        <div class="breadcrumb-v1 dark-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Product Detail</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li>Shop</li>
                            <li class="active">{{$product->product_name}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-70"></div>
 <div class="container">

             <div class="row">
                 <div class="col-sm-7 margin-b-30">
                     <!-- template -->
                     <div class="ms-showcase2-template">
                         <!-- masterslider -->
                         <div class="master-slider ms-skin-default" id="mastersliderr">

                             <div class="ms-slide">
                                 <img src="{{asset('img/original/'.$product->image)}}" data-src="{{asset('img/original/'.$product->image)}}" alt="lorem ipsum dolor sit"/>
                                 <img class="ms-thumb" src="{{asset('img/thumbnail/thumb_'.$product->image)}}" alt="thumb" />
                             </div>

                         </div>
                         <!-- end of masterslider -->
                     </div>
                     <!-- end of template -->
                 </div>
                 <div class="col-sm-4">
                    <div class="product-detail">
                        <h2>{{$product->carats}} cts {{$product->product_name}}</h2>
                        <span class="price">${{$product->price}}</span>
                        <p>
                            {!!$product->description!!}
                        </p>
                        <form class="cart">
                            <div class="add-to-cart-table">

                                <a href=""><button type="button" class="button">Add to cart</button></a>
                            </div>
                        </form>
                        <div class="space-30"></div>
                        <div class="cart-socials">
                            <ul class="list-inline">
                                <li><a href="#" data-toggle="tooltip" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" title="Share on FB"><i class="fa fa-facebook"></i></a></li>

                            </ul>
                        </div>
                        <div class="space-30"></div>
                        <ul class="list-unstyled text-left product-extra-info">
                            <li><span>Cut</span>{{$product->cut}}</li>
                            <li><span>Dimensions</span> {{$product->dimensions}}</li>
                            <li><span>Colour</span> <span class="label" style="background-color: {{$product->color}}"><i class="fa fa-diamond"></i></span></li>
                            <li><span>Category</span> <a href="#">{{$product->category_name}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="space-70"></div>
@endsection
