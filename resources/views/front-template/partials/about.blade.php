@extends('front-template.template')

@section('content')

        <div class="breadcrumb-v1 b-parllax">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 animated fadeInUp">
                        <h1>About us</h1>
                    </div>
                </div>
            </div>
        </div><!--breadcrumb end-->
        <div class="space-30"></div>
        <div class="container">
            <div class="row margin-b-40">
                <div class="col-xs-12 ">
                    <p class="">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div>
                        <ul class="nav nav-tabs tabs-default">
                            <li role="presentation" class="active"><a data-toggle="tab" role="tab" href="#tab-7" aria-expanded="true"> History</a></li>
                            <li role="presentation" class=""><a data-toggle="tab" role="tab" href="#tab-8" aria-expanded="false">Mission</a></li>
                        </ul>
                        <div class="tab-content tab-content-default">
                            <div id="tab-7" class="tab-pane active">

                                <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of
                                    existence in this spot, which was created for the bliss of souls like mine.</p>

                                <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at
                                    the present moment; and yet I feel that I never was a greater artist than now. When.</p>

                            </div>
                            <div id="tab-8" class="tab-pane">

                                <strong>Donec quam felis</strong>

                                <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                    and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                    sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="space-70"></div>
        </div><!--container end-->

        <div class="skin-bg process-style hide">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 margin-b-30">
                        <div class="process-box">
                            <span class="livicon " data-size="92" data-color="#fff" data-name="eye-open" data-duration="1000"></span>
                            <h2>01<span>.</span></h2>
                            <h4>Idea</h4>
                            <p>
                                Accusamus a, laboriosam autem animi similique iste harum doloribus quas fuga, minima error ea nihil eius repellat reprehenderit.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 margin-b-30">
                        <div class="process-box">
                            <span class="livicon " data-size="92" data-color="#fff" data-name="edit" data-duration="1000"></span>
                            <h2>02<span>.</span></h2>
                            <h4>Design</h4>
                            <p>
                                Accusamus a, laboriosam autem animi similique iste harum doloribus quas fuga, minima error ea nihil eius repellat reprehenderit.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 margin-b-30">
                        <div class="process-box">
                            <span class="livicon " data-size="92" data-color="#fff" data-name="wrench" data-duration="1000"></span>
                            <h2>03<span>.</span></h2>
                            <h4>Develop</h4>
                            <p>
                                Accusamus a, laboriosam autem animi similique iste harum doloribus quas fuga, minima error ea nihil eius repellat reprehenderit.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 margin-b-30">
                        <div class="process-box">
                            <span class="livicon " data-size="92" data-color="#fff" data-name="truck" data-duration="1000"></span>
                            <h2>04<span>.</span></h2>
                            <h4>Launch</h4>
                            <p>
                                Accusamus a, laboriosam autem animi similique iste harum doloribus quas fuga, minima error ea nihil eius repellat reprehenderit.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid no-padd hide">

            <div class="row no-margin">
                <div class="col-sm-3 no-padd text-center">
                    <div class="team-style-2">
                        <img src="images/team-1.jpg" alt="" class="img-responsive">
                        <div class="team-overlay">
                            <p>
                                <span>John Doe</span>
                                <em>Developer</em>
                            </p>
                        </div><!--team overlay end-->
                    </div><!--team style  end-->
                </div><!--col end-->
                <div class="col-sm-3 no-padd text-center">
                    <div class="team-style-2">
                        <img src="images/team-2.jpg" alt="" class="img-responsive">
                        <div class="team-overlay">
                            <p>
                                <span>John Doe</span>
                                <em>Developer</em>
                            </p>
                        </div><!--team overlay end-->
                    </div><!--team style  end-->
                </div><!--col end-->
                <div class="col-sm-3 no-padd text-center">
                    <div class="team-style-2">
                        <img src="images/team-3.jpg" alt="" class="img-responsive">
                        <div class="team-overlay">
                            <p>
                                <span>John Doe</span>
                                <em>Developer</em>
                            </p>
                        </div><!--team overlay end-->
                    </div><!--team style  end-->
                </div><!--col end-->
                <div class="col-sm-3 no-padd text-center">
                    <div class="team-style-2">
                        <img src="images/team-4.jpg" alt="" class="img-responsive">
                        <div class="team-overlay">
                            <p>
                                <span>John Doe</span>
                                <em>Developer</em>
                            </p>
                        </div><!--team overlay end-->
                    </div><!--team style  end-->
                </div><!--col end-->
            </div><!--row end-->
        </div>
        <div class="gray-bg">
            <div class="space-70"></div>
            <div class="container">
                <div class="heading-style-center">

                    <h2>Clients Say</h2>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <div class="owl-theme testimonial-slide">
                            <div class="item">
                                <p>
                                    We denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire
                                </p>
                                <h5>John Doe - Google inc.</h5>
                            </div>
                            <div class="item">
                                <p>
                                    We denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire
                                </p>
                                <h5>John Doe - Google inc.</h5>
                            </div>
                            <div class="item">
                                <p>
                                    We denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire
                                </p>
                                <h5>John Doe - Google inc.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-70"></div>
        </div>
        <div class="clients">
            <div class="space-40"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-theme client-slide">
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-1.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-3.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-2.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-3.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-1.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-2.png" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="images/client-3.png" alt="" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
