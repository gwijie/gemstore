@extends('front-template.template')

@section('content')

<div class="clearfix"></div>
        <div class="breadcrumb-v1 dark-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Gem Store</h1>
                        <p></p>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="/store"><i class="fa fa-home"></i></a></li>
                            <li class="active">Store</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-70"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-9 margin-b-30">
                    <p class="page-results">Showing 1-9 of 45 results</p>
                    <div class="row">
                      @foreach($products as $product)
                        <div class="col-sm-4 margin-b-30">
                            <div class="product-box">
                                <a href="/product/{{$product->product_id}}">
                                    <img src="{{asset('img/original/'.$product->image)}}" alt="{{$product->product_name}}" class="img-responsive full-img">
                                </a>
                                <div class="product-meta">
                                    <h4> <a href="/product/{{$product->product_id}}">{{$product->product_name}}</a></h4>
                                    <p><span class="price">${{$product->price}}</span><span class="more-info"><a href="/">View More</a></span></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <hr>
                    <div class="text-right hide">
                        <nav>
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    </div>
                </div>
                <!--sidebar-->
                <div class="col-md-3">
                    <div class="widget">
                        <h3>Search</h3>
                        <form>
                            <input type="text" class="form-control" placeholder="Searc &amp; hit enter">
                        </form>
                    </div><!--end widget-->

                    </div><!--end widget-->
                      <div class="widget">
                        <h3>Category</h3>
                        <ul class="list-inline tag-list">
                          @foreach($categories as $category)
                            <li><a href="/category/{{$category->cat_id}}">{{$category->category_name}}</a></li>
                          @endforeach
                        </ul>
                    </div><!--end widget-->
                </div>
                <!--sidebar end-->
            </div>
        </div>
@endsection
