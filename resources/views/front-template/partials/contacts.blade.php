@extends('front-template.template')

@section('content')

<div class="breadcrumb-v1 b-parllax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 animated fadeInUp">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
        </div><!--breadcrumb end-->
        <div class="space-70"></div>
        <div class="container">
            <div class="row margin-b-50">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p class="lead">Lorem ipsum dolor sit amet, <strong>consectetur adipisicing elit</strong>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7 margin-b-30">

                     <div class="contact-form">

                            <form role="form" id="feedbackForm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
<!--                                            <label class="control-label" for="name">Name *</label>-->
                                            <div>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" />

                                            </div>
                                            <span class="help-block" style="display: none;">Please enter your name.</span>
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
<!--                                            <label class="control-label" for="email">Email Address *</label>-->
                                            <div>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" />

                                            </div>
                                            <span class="help-block" style="display: none;">Please enter a valid e-mail address.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
<!--                                    <label class="control-label" for="message">Message *</label>-->
                                    <div>
                                        <textarea rows="5" cols="30" class="form-control" id="message" name="message" placeholder="Enter your message" ></textarea>

                                    </div>
                                    <span class="help-block" style="display: none;">Please enter a message.</span>
                                </div>
                                <img id="captcha" src="form/library/vender/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                <a href="#" onclick="document.getElementById('captcha').src = 'form/library/vender/securimage/securimage_show.php?' + Math.random();
                                    return false" class="btn btn-info btn-sm" style="margin-left: 10px;"><i class="fa fa-refresh"></i> </a><br/>
                                <div class="form-group" style="margin-top: 10px;">
<!--                                    <label class="control-label" for="captcha_code">Text Within Image *</label>-->
                                    <div>
                                        <input type="text" class="form-control" name="captcha_code" id="captcha_code" placeholder="For security, please enter the code displayed in the box." />

                                    </div>
                                    <span class="help-block" style="display: none;">Please enter the code displayed within the image.</span>
                                </div>
                                <span class="help-block" style="display: none;">Please enter a the security code.</span>
                                <input type="submit" id="feedbackSubmit" class="btn btn-skin btn-xl" style="display: block; margin-top: 10px;" value="submit">
                            </form>
                        </div><!--contact form-->

                </div>
                <div class="col-md-5">
                    <h3>Get In Touch</h3>
                    <p>
                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident,
                    </p>
                    <hr>
                    <p>
                      3rd Floor, Krishna Mansion; Suite 6S,<br>Moktar daddah St, Nairobi, Kenya
                    </p>
                      <hr>
                      <p>
                          <strong>E:</strong> info3@generationgemseastafrica.com<br>
                          <strong>P:</strong> +254 0700 00 00 00
                      </p>
                </div>
            </div>
        </div>
@endsection
