@extends('front-template.template')

@section('content')

        <div class="clearfix"></div>
        <div class="slider-main" style="overflow: hidden;">
            <!-- Master Slider -->
            <div class="master-slider ms-skin-default" id="masterslider">

                <!-- slide 1 -->
                <div class="ms-slide slide-1" data-delay="14">

                    <!-- slide background -->
                    <img src="{{asset('img/slider.jpg')}}" data-src="{{asset('img/slider.jpg')}}" alt="Slide1 background"/>
                    <h3 class="ms-layer sub-title3 one full-wid text-center white text-capitalize"
                        style="left:0; top:200px;"
                        data-type="text"
                        data-delay="1500"
                        data-duration="2000"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">It has finally Started</h3>
                    <h3 class="ms-layer title2 one full-wid font-sbold white text-uppercase text-center"
                        style="left:0;top: 240px;"
                        data-type="text"
                        data-delay="2000"
                        data-duration="2500"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">Huge Sale</h3>
                    <h5 class="ms-layer sub-title2 full-wid white text-uppercase text-center"
                        style="left:0px; top: 340px;"
                        data-type="text"
                        data-effect="bottom(45)"
                        data-duration="3000"
                        data-delay="2500"
                        data-ease="easeOutExpo">Upto 50% Off</h5>
                    <a class="ms-layer btn1 uppercase" href="#"
                       style="left: 530px; top:410px;"
                       data-type="text"
                       data-delay="3000"
                       data-ease="easeOutExpo"
                       data-duration="2000"
                       data-effect="scale(1.5,1.6)"> Shop Now <i class="fa fa-angle-right"></i> </a>
                    <a class="ms-layer btn1 uppercase" href="#"
                       style="left: 700px; top:410px;"
                       data-type="text"
                       data-delay="3200"
                       data-ease="easeOutExpo"
                       data-duration="2200"
                       data-effect="scale(1.5,1.6)"> Explore Now! <i class="fa fa-angle-right"></i></a> </div>
                <!-- end of slide -->

                <!-- slide 2 -->
                <div class="ms-slide slide-2" data-delay="14">

                    <!-- slide background -->
                    <img src="{{asset('img/slider1.jpg')}}" data-src="i{{asset('img/slider1.jpg')}}" alt="Slide1 background"/>
                    <h3 class="ms-layer sub-title3 text-uppercase"
                        style="left:110px; top:205px;"
                        data-type="text"
                        data-delay="1500"
                        data-duration="2000"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">Men's Shop</h3>
                    <h3 class="ms-layer title2  text-uppercase"
                        style="left:110px;top: 250px;"
                        data-type="text"
                        data-delay="2000"
                        data-duration="2500"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">40% Off on shoes</h3>
                    <h5 class="ms-layer sub-title2"
                        style="left:110px; top:345px;"
                        data-type="text"
                        data-effect="bottom(45)"
                        data-duration="3000"
                        data-delay="2500"
                        data-ease="easeOutExpo">This text is easy to change</h5>
                    <a class="ms-layer btn1" href="#"
                       style="left:110px; top:410px;"
                       data-type="text"
                       data-delay="3000"
                       data-ease="easeOutExpo"
                       data-duration="2000"
                       data-effect="scale(1.5,1.6)"> Shop Now <i class="fa fa-arrow-circle-right"></i>
                    </a> <a class="ms-layer btn1" href="#"
                            style="left:280px; top:410px;"
                            data-type="text"
                            data-delay="3200"
                            data-ease="easeOutExpo"
                            data-duration="2200"
                            data-effect="scale(1.5,1.6)"> More Details <i class="fa fa-arrow-circle-right"></i></a> </div>
                <!-- end of slide -->

                <!-- slide 1 -->
                <div class="ms-slide slide-3" data-delay="14">

                    <!-- slide background -->
                    <img src="{{asset('img/slider.jpg')}}" data-src="{{asset('img/slider.jpg')}}" alt="Slide1 background"/>
                    <h3 class="ms-layer sub-title3  full-wid text-center  text-capitalize"
                        style="left:0; top:200px;"
                        data-type="text"
                        data-delay="1500"
                        data-duration="2000"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">Ultimate multipurpose</h3>
                    <h3 class="ms-layer title2 one full-wid  text-uppercase text-center"
                        style="left:0;top: 240px;"
                        data-type="text"
                        data-delay="2000"
                        data-duration="2500"
                        data-ease="easeOutExpo"
                        data-effect="rotate3dtop(-100,0,0,40,t)">Easy to customize</h3>
                    <h5 class="ms-layer sub-title2 full-wid text-uppercase text-center"
                        style="left:0px; top: 340px;"
                        data-type="text"
                        data-effect="bottom(45)"
                        data-duration="3000"
                        data-delay="2500"
                        data-ease="easeOutExpo">Endless possibilities</h5>
                    <a class="ms-layer btn1 uppercase" href="#"
                       style="left: 530px; top:410px;"
                       data-type="text"
                       data-delay="3000"
                       data-ease="easeOutExpo"
                       data-duration="2000"
                       data-effect="scale(1.5,1.6)"> Shop Now <i class="fa fa-angle-right"></i> </a>
                    <a class="ms-layer btn1 uppercase" href="#"
                       style="left: 700px; top:410px;"
                       data-type="text"
                       data-delay="3200"
                       data-ease="easeOutExpo"
                       data-duration="2200"
                       data-effect="scale(1.5,1.6)"> Explore Now! <i class="fa fa-angle-right"></i></a> </div>
                <!-- end of slide -->
            </div>
            <!-- end Master Slider -->
        </div>

        <div class="space-70"></div>

        <div class='container'>
            <div class="row">
                <div class="col-sm-4 margin-b-30">
                    <div class="service-box">
                        <span class="livicon" data-size="64" data-name="credit-card" data-duration="1000" data-c="#aeaeae"  data-hc="#999"></span>
                        <h3>Secure payemnts</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 margin-b-30">
                    <div class="service-box">
                        <span class="livicon" data-size="64" data-name="mail" data-duration="1000" data-c="#aeaeae"  data-hc="#999"></span>
                        <h3>Full support</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 margin-b-30">
                    <div class="service-box">
                        <span class="livicon" data-size="64" data-name="gift" data-duration="1000" data-c="#aeaeae"  data-hc="#999"></span>
                        <h3>100% Originals</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-40"></div>


        <div class="new-arrivals">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="center-title margin-b-50">
                            <h2>Featured Products</h2>
                            <div class="border-style"></div>
                            <p>
                                text goes here
                            </p>
                        </div>
                    </div>
                </div>
                <!-- template -->
                <div class="ms-partialview-template" id="partial-view-1">
                    <!-- masterslider -->
                    <div class="master-slider ms-skin-default" id="new_arrivals">
                      @foreach($products as $product)
                        <div class="ms-slide">
                            <img src="{{asset('img/original/'.$product->image)}}" data-src="{{asset('img/original/'.$product->image)}}" alt="{{$product->product_name}}"/>
                            <div class="ms-info">
                                <h3><a href="/product/{{$product->product_id}}">{{$product->product_name}}</a></h3>
                                <p>${{$product->price}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- end of masterslider -->
                </div>
                <!-- end of template -->
            </div>
        </div>
        <div class="space-70"></div>


        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="center-title margin-b-50">
                        <h2>Our Products</h2>
                        <div class="border-style"></div>
                        <p>
                            text goes here
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
              @foreach($products as $product)
                <div class="col-sm-4 margin-b-30">
                    <div class="product-box">
                        <a href="#">
                            <img src="{{asset('img/original/'.$product->image)}}" alt="{{$product->product_name}}" class="img-responsive full-img">
                        </a>
                        <div class="product-meta">
                            <h4> <a href="#">{{$product->product_name}}</a></h4>
                            <p><span class="price">${{$product->price}}</span><span class="more-info"><a href="/product/{{$product->product_id}}">View More</a></span></p>
                            <a href="#" class="add_wishlist"><i class="icon-heart"></i></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div><!--end row-->
            <hr>
            <div class='text-center'>
                <a href='#' class="btn btn-xl btn-default">Load More Items...</a>
            </div> <hr>
        </div>

        <div class="space-70"></div>

        <div class="space-40"></div>
        <div class="testimonials">
            <div class="container">
                <div class="center-title margin-b-50">
                    <h2>Reviews</h2>
                    <div class="border-style"></div>

                </div>
                <div class="owl-carousel owl-theme owl-testi">
                    <div class="item">

                        <p>
                            " Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. "
                        </p>
                        <h5>John Doe</h5>
                    </div>
                    <div class="item">

                        <p>
                            " Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. "
                        </p>
                        <h5>John Doe</h5>
                    </div>
                    <div class="item">

                        <p>
                            " Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. "
                        </p>
                        <h5>John Doe</h5>
                    </div>
                    <div class="item">

                        <p>
                            " Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. "
                        </p>
                        <h5>John Doe</h5>
                    </div>
                    <div class="item">

                        <p>
                            " Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. "
                        </p>
                        <h5>John Doe</h5>
                    </div>
                </div>
            </div>
        </div>

@endsection
