<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 margin-b-30">
                <h3>Quick Links</h3>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About  </a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">Privacy & Policy</a></li>
                </ul>
            </div>

            <div class="col-md-3 margin-b-30">
                <h3>My Account</h3>
                <ul class="list-unstyled">
                    <li><a href="#">My orders</a></li>
                    <li><a href="#">Payment Checkout</a></li>
                </ul>
            </div>
            <div class="col-md-3 margin-b-30">
                <h3>Visit us</h3>
                <p>
                    3rd Floor, Krishna Mansion; Suite 6S,<br>Moktar daddah St, Nairobi, Kenya
                </p>
                <p>+01 345-676-5940</p>
                <p><a href="#">info@generationgemseastafrica.com</a></p>
                <div class="text-center">
                    <a class="social-icon social-dark social-rounded social-sm  si-colored-facebook" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a class="social-icon social-dark social-rounded social-sm si-colored-twitter" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a class="social-icon social-dark social-rounded social-sm si-colored-linkedin" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a>
                    <a class="social-icon social-dark social-rounded social-sm si-colored-g-plus" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Google+"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
            <div class="col-md-3 margin-b-30">
                <h3>Newsletter</h3>
                <p>
                    Subscribe to our newsletter and get 30% off on any products.
                </p>
                <form method="post" action="#" class="form-subscribe">
                    <input type="text" name="email" placeholder="Email Id..." class="form-control">

                    <button type="submit" name="submit" class="btn btn-skin btn-xl btn-block">Subscribe Now</button>
                </form>

            </div>

        </div>
        <div class="space-30"></div>
        <div class="footer-bottom">
            <div class="space-20"></div>
            <div class="row">
                <div class="col-sm-4 margin-b-20">
                    <a href="#"><img src="images/logo-dark.png" alt=""></a>
                </div>
                <div class="col-sm-4 margin-b-20">
                    <span>&copy; Copyright 2017. Designed from <i class="icon-heart"></i> by Creative Atlier</span>
                </div>
                <div class="col-sm-4 margin-b-20">
                    <img src="images/shop/payment.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
<!--back to top-->
<a href="#" class="scrollToTop"><span class="livicon " data-size="32" data-name="angle-double-up" data-duration="1000"></span></a>
<!--back to top end-->
<!-- jQuery plugins-->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('live-icon/raphael-min.js')}}"></script>
<script src="{{asset('live-icon/livicons-1.4.min.js')}}"></script>
<script src="{{asset('bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.counterup.min.js')}}" type="text/javascript" ></script>
<script src="{{asset('owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('bower_components/flexslider/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('js/jquery.appear.js')}}" type="text/javascript" ></script>
<script src="{{asset('js/modernizr.custom.97074.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.stellar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.BlackAndWhite.js')}}"></script>
<script src="{{asset('bower_components/lightbox2/dist/js/lightbox.min.js')}}"></script>
<script src="{{asset('js/jquery.hoverdir.js')}}"></script>
<script src="{{asset('js/jquery.sticky.js')}}"></script>
<script src="{{asset('bower_components/wow/dist/wow.min.js')}}"></script>
<script src="{{asset('js/headroom.js')}}"></script>
<script src="{{asset('js/mailchimp-custom.js')}}" type="text/javascript"></script>
<script src="{{asset('contact_form_2/contact.js')}}"></script>
<script src="{{asset('js/versa.js')}}" type="text/javascript"></script>
<!-- Master Slider -->
<script src="{{asset('masterslider/masterslider.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var slider = new MasterSlider();

        slider.control('arrows');
        slider.control('slideinfo', {insertTo: "#partial-view-1", autohide: false, align: 'bottom', size: 160});
        slider.control('circletimer', {color: "#FFFFFF", stroke: 9});

        slider.setup('new_arrivals', {
            width: 500,
            height: 281,
            space: 10,
            loop: true,
            view: 'fadeWave',
            layout: 'partialview'
        });
        (function ($) {
            "use strict";
            var slider = new MasterSlider();

            // adds Arrows navigation control to the slider.
            slider.control('arrows');
            slider.control('timebar', {insertTo: '#masterslider'});
            slider.control('bullets');

            slider.setup('masterslider', {
                width: 1400, // slider standard width
                height: 700, // slider standard height
                space: 0,
                layout: 'fullwidth',
                loop: true,
                preload: 0,
                instantStartLayers: true,
                autoplay: true
            });
        })(jQuery);

        $(".owl-testi").owlCarousel({
            autoPlay: 5000, //Set AutoPlay to 3 seconds
            navigation: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            pagination: false,
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]

        });
    });
</script>

<!-- Master Slider -->
<script type="text/javascript">
   var slider = new MasterSlider();

    slider.control('arrows');
    slider.control('thumblist', {autohide: false, dir: 'h', arrows: false, align: 'bottom', width: 127, height: 137, margin: 5, space: 5});

    slider.setup('mastersliderr', {
        width: 500,
        height:353,
        space: 5,
        view: 'scale',
        loop:true
    });



</script>
</body>
</html>
