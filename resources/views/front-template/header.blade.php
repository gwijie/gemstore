
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Generations Gems East Africa - Home</title>

        <!-- Bootstrap -->
        <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('et-line-icons/et-line.css')}}" rel="stylesheet">
        <link href="{{asset('bower_components/hover/css/hover-min.css')}}" rel="stylesheet">
        <link href="{{asset('css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('owl-carousel/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{asset('owl-carousel/owl.theme.css')}}" rel="stylesheet">
        <link href="{{asset('owl-carousel/owl.transitions.css')}}" rel="stylesheet">
        <link href="{{asset('bower_components/lightbox2/dist/css/lightbox.css')}}" rel="stylesheet">
        <link href="{{asset('bower_components/flexslider/flexslider.css')}}" rel="stylesheet">

        <!-- Base MasterSlider style sheet -->
        <link rel="stylesheet" href="{{asset('masterslider/style/masterslider.css')}}" />

        <!-- Master Slider Skin -->
        <link href="{{asset('masterslider/skins/default/style.css')}}" rel='stylesheet' type='text/css'>
        <!--main css file-->
        <link href="{{asset('css/shop.css')}}" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="preloader"></div>

        <!--top bar start-->
        <div class="top-bar-v1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-phone-square"></i> (+254) </a></li>
                            <li><a href="#"><i class="fa fa-envelope-square"></i> info@generationgemseastafrica.com</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 hidden-xs text-right">

                    </div>
                </div>
            </div>
        </div>
        <!--top bar end-->

        <!-- header -->
                <header class="top-header navbar-default navbar navbar-static-top sticky-header yamm">
                    <div class="search-click">
                        <div class="container">
                            <form>
                                <input type="text" class="form-control" placeholder="Type and hit enter">
                                <i class="fa fa-times"></i>
                            </form>
                        </div>
                    </div><!--search bar end-->
                    <div class="container">
                        <div class="pull-right nav-header-right">

                            <div class="h-item dropdown-search" id="search-trigger">
                                <a href="javascript:void(0)"><i class="fa fa-search"></i></a>

                            </div>
                            <div class="h-item dropdown">
                                <a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-bag"></i> <span class="badge">1</span></a>
                                <ul class="dropdown-menu h-item-dropdown cart-dropdown">
                                    <li class="clearfix">3 items <span class="total">$597.00</span></li>
                                    <li class="clearfix cart-footer">
                                        <a href="#" class="btn btn-dark btn-lg">View Cart</a>
                                        <a href="#" class="btn btn-skin btn-lg">Checkout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/"><i class="fa fa-diamond fa-2x"></i> {{ config('app.name') }}</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <li class="dropdown">
                                    <a href="/" class=""  role="button" aria-haspopup="false">Home </a>
                                </li>

                                <li class="dropdown">
                                    <a href="/about" class=""  role="button" aria-haspopup="false">About Us </a>
                                </li>

                                <li class="dropdown">
                                    <a href="/store" class=""  role="button" aria-haspopup="false">Store</a>
                                </li>

                                <li class="dropdown">
                                    <a href="/contact" class=""  role="button" aria-haspopup="false">Contact Us </a>
                                </li>


                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </header>
                <!-- header end -->
