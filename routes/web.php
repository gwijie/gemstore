<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/about', 'FrontController@about');
Route::get('/store', 'FrontController@store');
Route::get('/contact', 'FrontController@contact');
Route::get('/product/{id}', 'FrontController@product_detail');

Route::get('/login', 'Auth\LoginController@index');
Route::post('/authenticate', 'Auth\LoginController@postLogin');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['authenticated']], function () {

      Route::get('/dashboard', 'DashboardController@index');

      Route::get('/admin/add_product', 'ProductController@add_products');
      Route::get('/admin/view_products', 'ProductController@add_product');
      Route::post('/add_products', 'ProductController@store');
      Route::get('/admin/view_products', 'ProductController@show');
      Route::get('/admin/view_product/{id}', 'ProductController@showItem');
      Route::get('/admin/product/edit/{id}', 'ProductController@editItem');
      Route::post('/update_product/{id}', 'ProductController@update_product');
      Route::get('/admin/add_category', 'CategoryController@add_category');
      Route::post('/add_category', 'CategoryController@insert_category');

});
